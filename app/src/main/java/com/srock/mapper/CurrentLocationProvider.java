package com.srock.mapper;

import android.annotation.SuppressLint;
import android.location.Location;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnSuccessListener;

import org.osmdroid.views.overlay.mylocation.IMyLocationConsumer;
import org.osmdroid.views.overlay.mylocation.IMyLocationProvider;

/**
 * Created by pawelsrokowski on 14/11/2017.
 */

public class CurrentLocationProvider implements IMyLocationProvider {

    private FusedLocationProviderClient locationClient;
    private Location location;

    public CurrentLocationProvider(FusedLocationProviderClient locationClient) {
        super();
        this.locationClient = locationClient;
        this.locationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                this.location = location;
            }
        });
    }

    @Override
    public boolean startLocationProvider(IMyLocationConsumer myLocationConsumer) {
        return true;
    }

    @Override
    public void stopLocationProvider() {

    }

    @Override
    public Location getLastKnownLocation() {
        return location;
    }

    @Override
    public void destroy() {

    }
}
